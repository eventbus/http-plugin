package com.bitbucket.eventbus.http;

import com.bitbucket.eventbus.http.utils.Wrapper;
import com.bitbucket.eventbus.interfaces.Pluggable;
import com.google.gson.Gson;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Response;

import java.util.concurrent.Future;

import static org.asynchttpclient.Dsl.asyncHttpClient;


public class HttpPlugin implements Pluggable {
  private final String host;
  private final Integer port;
  private final ConnectionType type;
  private final AsyncHttpClient client;

  public enum ConnectionType{
    HTTP,
    WS
  }

  public HttpPlugin(String host, Integer port, ConnectionType type) {
    this.host = host;
    this.port = port;
    this.type = type;
    this.client = asyncHttpClient();
  }

  @Override
  public void process(String channel, String event, Object data) {
    String json = new Gson().toJson(new Wrapper(channel, event, data));

    String url =
      (type == ConnectionType.WS ? "ws://" : "") + (host) +
      (port != null ? ":" + port : "");
    Future<Response> whenResponse = client.preparePost(url)
      .setBody(json)
    .execute();
  }
}
