package com.bitbucket.eventbus.http.utils;

public class Wrapper {
  String channel;
  String event;
  Object data;

  public Wrapper(String channel, String event, Object data) {
    this.channel = channel;
    this.event = event;
    this.data = data;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }
}
